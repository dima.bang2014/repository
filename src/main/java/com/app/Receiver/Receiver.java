package com.app.Receiver;

import com.app.Domain.Sensors;
import com.app.Sender.Sender;
import com.app.Service.SensorsService;
import org.springframework.amqp.rabbit.annotation.RabbitHandler;
import org.springframework.amqp.rabbit.annotation.RabbitListener;
import org.springframework.beans.factory.annotation.Autowired;

import static java.lang.Integer.parseInt;

@RabbitListener(queues = "c2r")
public class Receiver {


    @Autowired
    SensorsService sensorsService;

    @Autowired
    Sensors sensors;

    @Autowired
    Sender sender;

    @RabbitHandler
    public void receive(String message) {
        String[] buffer = message.split("-");

        sensors.setId(parseInt(buffer[0]));
        sensors.setRoom(parseInt(buffer[1]));
        sensors.setName(buffer[2]);
        sensors.setSenstype(buffer[3]);

        sensorsService.saveOrUpdate(sensors);
        System.out.println(" [x] Received '" +
                sensors.getId()+"-"+
                sensors.getRoom()+"-"+
                sensors.getName()+"-"+
                sensors.getSenstype()+"'");

        sender.send("created field with {" +
                "id: " + sensors.getId()+
                ", room: " + sensors.getRoom()+
                ", name: " + sensors.getName()+
                ", senstype: " + sensors.getSenstype());
    }
}
