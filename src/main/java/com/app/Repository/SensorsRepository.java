package com.app.Repository;

import org.springframework.data.repository.CrudRepository;
import com.app.Domain.Sensors;

public interface SensorsRepository extends CrudRepository<Sensors, Integer>
{
}

