package com.app;

import com.app.Domain.Sensors;
import com.app.Receiver.Receiver;
import com.app.Sender.Sender;
import org.springframework.amqp.core.Queue;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
public class Config {

    @Bean
    public Queue r2c() {
        return new Queue("r2c");
    }

    @Bean
    public Sensors getNewSensors() {
        return new Sensors();
    }

    @Bean
    public Sender getNewSender() {
        return new Sender();
    }

    @Bean
    public Receiver getNewReceiver() {
        return new Receiver();
    }
}