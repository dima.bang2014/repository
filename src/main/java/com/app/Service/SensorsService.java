package com.app.Service;

import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.app.Domain.Sensors;
import com.app.Repository.SensorsRepository;
//defining the business logic
@Service
public class SensorsService
{
    @Autowired
    SensorsRepository sensorsRepository;

    public List<Sensors> getAllSensors()
    {
        List<Sensors> sensors = new ArrayList<Sensors>();
        sensorsRepository.findAll().forEach(sensor -> sensors.add(sensor));
        return sensors;
    }
    //getting a specific record
    public Sensors getsensorsById(int id)
    {
        return sensorsRepository.findById(id).get();
    }
    public void saveOrUpdate(Sensors sensors)
    {
        sensorsRepository.save(sensors);
    }
    //deleting a specific record
    public void delete(int id)
    {
        sensorsRepository.deleteById(id);
    }
}